# Deployment Check List Nova

## Check Build environment and Test build application :

This involves ensuring that the build server has the necessary software and components installed to run the build process. For instance, you will check the Node.js version, database version, etc., to ensure they are compatible with the application requirements.

- [ ] Check installed components on the build server that allows you to build the app (node version, database version,…)

- [ ] Run the build script.

This process is about executing the build script to generate the application. It involves compiling the code, bundling the assets, and creating executable binaries.

- [ ] Check that the build script is run correctly and 0 errors 0 warnings are logged.

Post the build, you would ensure the build has been successful with zero errors or warnings. This can involve checking log files or build reports that have been generated during the build process.

## Run… Unitary Test & Quality check :

In this step, you will execute the unit tests for the project using SonarQube and ChatGPT. These tests are designed to test individual units of code (like functions or methods) to ensure they perform as expected.

- [ ] Run the test script with [**Sonnar**](https://gitlab.com/sqoin_team/sonarqube/-/blob/main/run.sh) and [**ChatGPT**](https://github.com/sqoin/analyze_chatgpt) that will execute all the unit tests in the project.

Here, you will run a code quality check to identify potential issues related to best practices and security. This can involve using a static code analyzer to scan the codebase.

- [ ] Check that the test script is run correctly and 0 errors 0 warnings are logged.
- [ ] Run the script that will scan the code for best practices and security.
- [ ] Check that 0 concerns are logged.

After running the unit tests, check the test reports to ensure there are no errors or warnings. This helps to confirm the code is working as expected at a unit level.

## Run Script for Test Deployment:

- [ ] Run the deployment script in test mode.

Create a test environment mirroring the production one where the newly built application is deployed. This environment is for the development team to check if the code is working correctly without any errors.

- [ ] Notify dev team (bot / email) that a new version live in test mode.

Upon successful deployment in the test environment, you will notify the development team through a bot or email. This informs them that a new version of the application is live in test mode for them to review.

## Run the acceptance Test:

- [ ] Run the acceptance test with a bot & then a human [**Selenium**](https://gitlab.com/sqoin_team/selenium_nova.git)

Run acceptance tests, which are typically broader than unit tests
and are designed to simulate real user scenarios. These tests are usually executed by automated bots and then further validated by humans.

- [ ] Check that 0 errors are reported after running acceptance tests.

After running the acceptance tests, you would verify if there are any errors reported. This step helps ensure that the application is behaving as expected in scenarios that mimic real user behavior.

- [ ] Notify the dev team about test results

Notify Dev Team about Test Results: Finally, notify the development team about the test results, including any errors or issues found during the acceptance tests. This allows them to make any necessary fixes before the application is released to the production environment.

## Run Security  Test After The deployment:

- [ ] Vulnerability Scanners: These tools scan your website for common security vulnerabilities such as outdated software versions, misconfigurations, and known vulnerabilities. 

- [ ] Web Application Firewalls (WAFs): WAFs can protect your website from common web attacks, such as cross-site scripting (XSS), SQL injection, and distributed denial-of-service (DDoS) attacks. Popular WAF solutions include Cloudflare, ModSecurity, and Sucuri.

- [ ] Penetration Testing Tools: These tools simulate real-world attacks on your website to identify vulnerabilities and weaknesses. They provide a more in-depth assessment of your website's security. Examples include Burp Suite, OWASP ZAP, and Metasploit.

- [ ] SSL/TLS Checkers: These tools analyze the SSL/TLS configuration of your website to ensure secure communication between your server and visitors' browsers. SSL Labs (Qualys) and SSL Checker (DigiCert) are commonly used tools for this purpose.

- [ ] Security Headers Checkers: These tools evaluate the HTTP response headers of your website to determine if the necessary security headers are in place. Examples include Security Headers and Mozilla Observatory.

- [ ] Content Security Policy (CSP) Analyzers: CSP is a security feature that helps protect against cross-site scripting (XSS) and other code injection attacks. Tools like CSP Evaluator and Report URI can help you analyze and generate proper CSP policies.

- [ ] Code Review Tools: These tools analyze your website's source code to identify potential security vulnerabilities, coding errors, and best practice violations. Popular code review tools include SonarQube, Checkmarx, and Veracode.

[document](https://docs.google.com/document/d/1FGg1E1vwzicaYgpa9OIg9w5aVDh48-2J2bMEC6Gz20s)
